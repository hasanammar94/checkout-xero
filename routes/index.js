var express = require('express');
var router = express.Router();
var request = require('request');
var config = require('../config');

router.get('/', function (req, res) {
  var options = {
    url: 'https://sandbox.checkout.com/api2/v2/tokens/payment',
    method: 'POST',
    headers: {
      'Authorization': config.secret_key,
      'Content-Type': 'application/json'
    },
    json: {
      currency: req.query.currency,
      value: parseFloat(req.query.amount),
      udf1: req.query.invoiceNo
    }
  };

  request(options, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log(response.statusCode, body);
      var payment_token = body.id;
      var publicKey = config.pub_key;
      res.render('index', { query: req.query, publicKey: publicKey, payment_token: payment_token });
    }
  });
});

router.post('/', function (req, res) {
  console.log("req.body:",req.body);
  console.log(req.body.paymentToken);
  request.get({
    url: 'https://sandbox.checkout.com/api2/v2/charges/'+req.body.paymentToken,
    headers: { 'Authorization': config.secret_key }
  }, function (error, response, body) {
    if (error) {
      console.log(error);
    } else {
      console.log(response.statusCode, body);
      if(response.statusCode == 200)
        res.send('Charge Verified');
      else res.send('Charge Not Verified');
    }
  });
});

module.exports = router;
